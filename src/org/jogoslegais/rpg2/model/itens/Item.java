/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jogoslegais.rpg2.model.itens;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.Random;
import org.bigolin.ActiveRecord;
import org.bigolin.Conexao;

/**
 *
 * @author aluno
 */
public class Item implements ActiveRecord {

    private ItemType tipo;

    private ArrayList<Atributo> atributos = new ArrayList<>();
    private ArrayList<Atributo> atributosOffline = new ArrayList<>();

    private String nome, desc, hist, image;
    
    private boolean equiped = false;
    
    private int id = -1, money, peso;

    public static Item random() {

        Random r = new Random();

        String[] sufixos = new String[]{
            "do caos",
            "da justiça",
            "topzera",
            "overpower",
            "do mal",
            "angelical"
        }, atributos = new String[]{
            "for",
            "des",
            "int"
        }, aneis = new String[]{
            "/res/img/anel/1.png"
        }, armaduras = new String[]{
            "/res/img/armadura/1.png"
        }, pocoes = new String[]{
            "/res/img/pocao/1.png"
        };

        EnumMap<ItemType, String[]> imgs = new EnumMap<>(ItemType.class);

        imgs.put(ItemType.Anel, aneis);
        imgs.put(ItemType.Armadura, armaduras);
        imgs.put(ItemType.Pocao, pocoes);

        // Numero de atributos
        int numAtr = r.nextInt(100);

        if (numAtr < 50) {
            numAtr = 1;
        } else if (numAtr < 85) {
            numAtr = 2;
        } else {
            numAtr = 3;
        }

        Item i = new Item();

        ItemType tipo = ItemType.fromInt(r.nextInt(ItemType.values().length));
        String nome = tipo + " " + sufixos[r.nextInt(sufixos.length)];

        int preco = 0;

        String img = imgs.get(tipo)[r.nextInt(imgs.get(tipo).length)];

        if (tipo == ItemType.Pocao) {
            atributos = new String[]{"Cura Mana", "Cura Vida"};
        }

        for (int j = 0; j < numAtr; j++) {
            Atributo a = new Atributo(atributos[r.nextInt(atributos.length)], r.nextInt(10) + 1);
            i.addAtributo(a);
            preco += a.getIntensidade() * (j + 1);
        }

        i.setImage(img);

        i.setNome(nome);
        i.setTipo(tipo);
        i.setMoney(preco);

        return i;
    }
        
    public boolean isEquiped() {
        return equiped;
    }

    public void setEquiped(boolean equiped) {
        this.equiped = equiped;
    }


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;

        for (Atributo a : getAtributos()) {
            a.setIdItem(id);
        }
    }

    private boolean podeSerRestaurado; // X

    public void addAtributo(Atributo a) {

        a.setIdItem(id);

        for (Atributo at : atributos) {
            if (a.getNome().equals(at.getNome())) {
                at.setIntensidade(at.getIntensidade() + a.getIntensidade());
                return;
            }
        }

        for (Atributo at : atributosOffline) {
            if (a.getNome().equals(at.getNome())) {
                at.setIntensidade(at.getIntensidade() + a.getIntensidade());
                return;
            }
        }

        atributosOffline.add(a);
    }

    public String mostraBonus() {
        String res = "";
        for (Atributo a : getAtributos()) {
            res += a.getNome() + " +" + a.getIntensidade() + "\n";
        }

        return res;
    }

    public int getAtributo(String nome) {
        for (Atributo a : atributos) {
            if (a.getNome().equals(nome)) {
                return a.getIntensidade();
            }
        }

        for (Atributo a : atributosOffline) {
            if (a.getNome().equals(nome)) {
                return a.getIntensidade();
            }
        }

        return 0;
    }

    private ArrayList<Atributo> getAtributos() {
        ArrayList<Atributo> res = new ArrayList<>();

        res.addAll(atributos);
        res.addAll(atributosOffline);

        return res;
    }

    public ItemType getTipo() {
        return tipo;
    }

    public void setTipo(ItemType tipo) {
        this.tipo = tipo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getHist() {
        return hist;
    }

    public void setHist(String hist) {
        this.hist = hist;
    }

    public int getPeso() {
        return peso;
    }

    public void setPeso(int peso) {
        this.peso = peso;
    }

    public boolean isPodeSerRestaurado() {
        return podeSerRestaurado;
    }

    public void setPodeSerRestaurado(boolean podeSerRestaurado) {
        this.podeSerRestaurado = podeSerRestaurado;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        if (money < 0) {
            throw new ItemInvalidoException();
        } else {
            this.money = money;
        }

    }

    @Override
    public boolean insert() {
        String selectSQL = "INSERT INTO item VALUES(ITEM_SEQ.NEXTVAL, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, tipo.toInt());
            ps.setInt(2, podeSerRestaurado ? 1 : 0);
            ps.setInt(3, money);
            ps.setInt(4, peso);
            ps.setString(5, nome);
            ps.setString(6, desc);
            ps.setString(7, hist);
            ps.setString(8, image);
            ps.setInt(9, equiped ? 1 : 0);

            ps.executeUpdate();

            ps = dbConnection.prepareStatement("SELECT ITEM_SEQ.CURRVAL FROM DUAL ");
            ResultSet rs = ps.executeQuery();
            rs.next();
            setId(rs.getInt("CURRVAL"));

            for (Atributo a : atributosOffline) {
                a.insert();
            }

            for (Atributo a : atributos) {
                a.update();
            }
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        atributos.addAll(atributosOffline);
        atributosOffline.clear();

        return true;
    }

    @Override
    public boolean delete() {
        String selectSQL = "DELETE FROM item WHERE id = ?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {

            for (Atributo a : atributos) {
                a.delete();
            }

            ps = dbConnection.prepareStatement("DELETE FROM itens_jogador WHERE id_item = ?");
            ps.setInt(1, id);
            ps.executeUpdate();

            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, id);

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return true;
    }

    @Override
    public boolean update() {

        System.out.println("id: " + id);

        if (id < 0) {
            return insert();
        }

        String selectSQL = "UPDATE item SET id_tipo=?, pode_ser_restaurado=?, money=?, peso=?, nome=?, descr=?, histo=?, image=?, equiped=? WHERE id=?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, tipo.toInt());
            ps.setInt(2, podeSerRestaurado ? 1 : 0);
            ps.setInt(3, money);
            ps.setInt(4, peso);
            ps.setString(5, nome);
            ps.setString(6, desc);
            ps.setString(7, hist);
            ps.setString(8, image);
            ps.setInt(9, equiped ? 1 : 0);
            ps.setInt(10, id);

            ps.executeUpdate();

            for (Atributo a : atributos) {
                a.update();
            }
            for (Atributo a : atributosOffline) {
                a.insert();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        atributos.addAll(atributosOffline);
        atributosOffline.clear();

        return true;
    }

    @Override
    public void load() {
        String selectSQL = "SELECT * FROM item WHERE id = ?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, this.id);

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                setTipo(ItemType.fromInt(rs.getInt("id_tipo")));
                setPodeSerRestaurado(rs.getInt("pode_ser_restaurado") != 0);
                setMoney(rs.getInt("money"));
                setPeso(rs.getInt("peso"));
                setNome(rs.getString("nome"));
                setDesc(rs.getString("descr"));
                setHist(rs.getString("histo"));
                setImage(rs.getString("image"));
                setEquiped(rs.getInt("equiped") != 0);
                
                loadAtributos(c);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void load(int id) {
        setId(id);
        load();
    }

    private void loadAtributos(Conexao c) {
        String selectSQL = "SELECT * FROM atributo where id_item = ?";

        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, this.id);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String nomeAt = rs.getString("nome");
                int intensidade = rs.getInt("intensidade");

                Atributo a = new Atributo(nomeAt, intensidade);

                a.setIdItem(id);

                atributos.add(a);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
