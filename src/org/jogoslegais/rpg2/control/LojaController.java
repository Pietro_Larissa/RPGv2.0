package org.jogoslegais.rpg2.control;

import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import org.jogoslegais.rpg2.model.itens.Atributo;
import org.jogoslegais.rpg2.model.jogador.Jogador;
import org.jogoslegais.rpg2.model.itens.Item;
import org.jogoslegais.rpg2.model.itens.ItemType;
import org.jogoslegais.rpg2.model.itens.Loja;

/**
 * FXML Controller class
 *
 * @author marcio
 */
public class LojaController implements Initializable {

    @FXML
    private ListView<Item> itens;
    private ObservableList<Item> itensObs;

    @FXML
    private ListView<Item> itensJog;
    private ObservableList<Item> itensJogObs;

    Loja loja = new Loja();

    @FXML
    private Label nomeItem;
    @FXML
    private Label preco;
    @FXML
    private Label bonus;
    @FXML
    private Label dinheiro;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        itensObs = itens.getItems(); //Ponteiro
        itensJogObs = itensJog.getItems(); // Ponteiro

        this.cellFactory();

        Jogador jog = new Jogador();
        jog.load(0);

        loja.setJog(jog);
        loja.load(0);

        this.itensObs.addAll(loja.getItens());
        this.itensJogObs.addAll(loja.getJog().getInventario());

        dinheiro.setText("Você tem: " + loja.getJog().getMoney() + "G");
    }

    private void cellFactory() {
        itens.setCellFactory(param -> new ListCell<Item>() {
            private ImageView imageView = new ImageView();

            @Override
            public void updateItem(Item item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                    setText("");
                    setGraphic(null);
                } else {
                    System.out.println(item.getImage());
                    if (item.getImage() != null) {
                        imageView.setImage(new Image(item.getImage()));
                    }
                    setText(item.getNome());
                    setGraphic(imageView);
                }
            }
        });
        itensJog.setCellFactory(param -> new ListCell<Item>() {
            private ImageView imageView = new ImageView();

            @Override
            public void updateItem(Item item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                    setText("");
                    setGraphic(null);
                } else {
                    if (item.getImage() != null) {
                        imageView.setImage(new Image(item.getImage()));
                    }
                    setText(item.getNome());
                    setGraphic(imageView);
                }
            }
        });
    }

    @FXML
    public void updateLoja() {
        Item i = itens.getSelectionModel().getSelectedItem();

        if (i == null) {
            return;
        }

        nomeItem.setText(i.getNome());
        preco.setText(i.getMoney() + "G");
        bonus.setText(i.mostraBonus());
        dinheiro.setText("Você tem: " + loja.getJog().getMoney() + "G");
    }

    @FXML
    public void updateJog() {
        Item i = itensJog.getSelectionModel().getSelectedItem();

        if (i == null) {
            return;
        }

        nomeItem.setText(i.getNome());
        preco.setText((i.getMoney() / 2) + "G");
        bonus.setText(i.mostraBonus());
        dinheiro.setText("Você tem: " + loja.getJog().getMoney() + "G");
    }

    @FXML
    public void comprar() {

        Item i = itens.getSelectionModel().getSelectedItems().get(0);

        // Se o cara não selecionou nada, 
        // não compre o vazio neh mew
        if (i == null) {
            return;
        }

        if (loja.comprar(i)) {
            dinheiro.setText("Você tem: " + loja.getJog().getMoney() + "G");

            itensObs.remove(i);
            itensJogObs.add(i);

            nomeItem.setText(geraFraseCompra());
            preco.setText("");
            dinheiro.setText("");

            loja.update();
            loja.getJog().update();
        } else {
            nomeItem.setText(geraFraseSemDinheiro());
            preco.setText("");
            dinheiro.setText("");
        }
    }

    private String geraFraseSemDinheiro() {

        String[] frases = new String[]{
            "Desculpe, mas não fazemos fiado.",
            "Pode ir tirando o cavalinho da chuva!"
        };

        Random r = new Random();

        return frases[r.nextInt(frases.length)];
    }

    private String geraFraseCompra() {

        String[] frases = new String[]{
            "Obrigada!",
            "Trato feito!"
        };

        Random r = new Random();

        return frases[r.nextInt(frases.length)];
    }

    @FXML
    public void vender() {

        Item i = itensJog.getSelectionModel().getSelectedItem();

        // Se o cara não selecionou nada, 
        // não compre o vazio neh mew
        if (i == null) {
            return;
        }

        // Se a compra não foi be-sucedida,
        // pula fora
        if (!loja.vender(i)) {
            return;
        }

        nomeItem.setText(geraFraseCompra());
        preco.setText("");
        dinheiro.setText("");

        itensObs.add(i);
        itensJogObs.remove(i);

        loja.update();
        loja.getJog().update();
    }

    @FXML
    public void jogarFora() {
        Item i = itensJog.getSelectionModel().getSelectedItem();

        if (i == null) {
            return;
        }

        loja.getJog().removeItem(i);

        i.delete();

        itensJogObs.remove(i);
    }
}
