/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jogoslegais.rpg2.model.itens;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.bigolin.ActiveRecord;
import org.bigolin.Conexao;

/**
 *
 * @author pietro
 */
public class Atributo implements ActiveRecord {

    private String nome = "";
    private int intensidade = 0, idItem = -1;

    public int getIdItem() {
        return idItem;
    }

    public void setIdItem(int id) {
        this.idItem = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIntensidade() {
        return intensidade;
    }

    public void setIntensidade(int intensidade) {
        this.intensidade = intensidade;
    }

    public Atributo(String nome, int intensidade) {
        this.nome = nome;
        this.intensidade = intensidade;
    }

    @Override
    public boolean insert() {
        String selectSQL = "INSERT INTO atributo VALUES(?, ?, ?)";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setString(1, nome);
            ps.setInt(2, intensidade);
            ps.setInt(3, this.idItem);

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return true;
    }

    @Override
    public boolean delete() {
        String selectSQL = "DELETE FROM atributo WHERE id_item = ? and nome = ?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, this.idItem);
            ps.setString(2, nome);

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return true;
    }

    @Override
    public boolean update() {

        String selectSQL = "UPDATE atributo SET intensidade = ? WHERE id_item = ? and nome = ?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, intensidade);
            ps.setInt(2, this.idItem);
            ps.setString(3, nome);

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return true;
    }

    @Override
    public void load() {
        String selectSQL = "SELECT * FROM atributo WHERE id_item = ? and nome = ?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, this.idItem);
            ps.setString(2, nome);

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                setIntensidade(rs.getInt("intensidade"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    // precisa dar setnome antes de chamar esse metodo daqui
    public void load(int id) {
        setIdItem(id);
        load();
    }
}
