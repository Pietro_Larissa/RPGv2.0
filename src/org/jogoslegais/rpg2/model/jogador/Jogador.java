/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jogoslegais.rpg2.model.jogador;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.jogoslegais.rpg2.model.itens.ItemType;
import org.jogoslegais.rpg2.model.itens.Item;
import java.util.ArrayList;
import java.util.EnumMap;
import org.bigolin.ActiveRecord;
import org.bigolin.Conexao;

/**
 * i
 *
 * @author Pietro Carrara
 * @author Larissa da Rosa
 */
public class Jogador implements ActiveRecord {

    private static final int MAX_ANEL = 10, MAX_ARMADURA = 3, MAX_POCAO = 20;

    // Limites de itens que podem ser segurados pelo jogador
    private EnumMap<ItemType, Integer> limites = new EnumMap<ItemType, Integer>(ItemType.class);

    private int id = -1, money, forca, dest, inte, vidaAtual, manaAtual, vidaMaxima, manaMaxima;

    // Muita atenção agora, porque não é fácil!
    // Isso cria uma "matriz", onde a primeira parte
    // vc fala um Tipo de item, e a segunda a posição 
    // do item. Então se eu quero ver as armaduras que
    // o cara ta usando, eu passo o ItemType.Armadura pra
    // acessar aquela ArrayList.
    private EnumMap<ItemType, ArrayList<Item>> equipados = new EnumMap<ItemType, ArrayList<Item>>(ItemType.class);

    private ArrayList<Item> inventario = new ArrayList<Item>();
    private ArrayList<Item> inventarioOffline = new ArrayList<>();

    public Jogador() {
        // Inicializar os arraylists 
        equipados.put(ItemType.Anel, new ArrayList<Item>());
        equipados.put(ItemType.Armadura, new ArrayList<Item>());
        equipados.put(ItemType.Pocao, new ArrayList<Item>());

        // Limites dos itens
        limites.put(ItemType.Anel, MAX_ANEL);
        limites.put(ItemType.Armadura, MAX_ARMADURA);
        limites.put(ItemType.Pocao, MAX_POCAO);
    }

    public int getVidaAtual() {
        return vidaAtual;
    }

    public void setVidaAtual(int vidaAtual) {
        this.vidaAtual = vidaAtual;
    }

    public int getManaAtual() {
        return manaAtual;
    }

    public void setManaAtual(int manaAtual) {
        this.manaAtual = manaAtual;
    }

    public int getVidaMaxima() {
        return vidaMaxima;
    }

    public void setVidaMaxima(int vidaMaxima) {
        this.vidaMaxima = vidaMaxima;
    }

    public int getManaMaxima() {
        return manaMaxima;
    }

    public void setManaMaxima(int manaMaxima) {
        this.manaMaxima = manaMaxima;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public void addItem(Item i) {
        inventarioOffline.add(i);
    }

    public void removeItem(Item i) {
        inventario.remove(i);
        inventarioOffline.remove(i);
    }

    public int getForca() {

        int resultado = forca;

        // Para todas as armaduras, anéis, e etc...
        for (ItemType t : ItemType.values()) {
            for (Item i : equipados.get(t)) {
                resultado += i.getAtributo("for");
            }
        }

        return resultado;
    }

    public void setForca(int forca) {
        this.forca = forca;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public int getDest() {

        int resultado = dest;

        // Para todas as armaduras, anéis, e etc...
        for (ItemType t : ItemType.values()) {
            for (Item i : equipados.get(t)) {
                resultado += i.getAtributo("des");
            }
        }

        return resultado;
    }

    public void setDest(int dest) {
        this.dest = dest;
    }

    public int getInte() {

        int resultado = inte;

        // Para todas as armaduras, anéis, e etc...
        for (ItemType t : ItemType.values()) {
            for (Item i : equipados.get(t)) {
                resultado += i.getAtributo("int");
            }
        }

        return resultado;
    }

    public void setInte(int inte) {
        this.inte = inte;
    }

    public ArrayList<Item> getInventario() {

        ArrayList<Item> res = new ArrayList<>();

        res.addAll(inventario);
        res.addAll(inventarioOffline);

        return res;
    }

    public void setInventario(ArrayList<Item> inventario) {
        this.inventario = inventario;
    }

    public ArrayList<Item> getEquippedItems() {
        ArrayList<Item> itens = new ArrayList<Item>();

        // Para todas as armaduras, anéis, e etc...
        for (ItemType t : ItemType.values()) {
            for (Item i : equipados.get(t)) {
                itens.add(i);
            }
        }

        return itens;
    }

    public boolean usarPocao(Item i) {
        // Se não é uma poção,
        // deu ruim
        if (i.getTipo() != ItemType.Pocao) {
            return false;
        }

        // Se a gente não consegui tirar
        // a poção do cinto, deu ruim
        if (!equipados.get((ItemType.Pocao)).remove(i)) {
            return false;
        }

        curaMana(i.getAtributo("curaMana"));
        curaVida(i.getAtributo("CuraVida"));

        return true;
    }

    public void curaVida(int cura) {
        if (cura + vidaAtual > vidaMaxima) {
            vidaAtual = vidaMaxima;
        } else {
            vidaAtual += cura;
        }
    }

    public void curaMana(int cura) {
        if (cura + manaAtual > manaMaxima) {
            manaAtual = manaMaxima;
        } else {
            manaAtual += cura;
        }
    }

    public boolean equipar(Item i) {
        
        update();
        
        // Se o item não está no inventário,
        // deu errado.
        if (!inventario.remove(i)) {
            return false;
        }

        // Sa para aquele tipo de item o limite
        // foi estrapolado, deu errado
        if (equipados.get(i.getTipo()).size() >= limites.get(i.getTipo())) {
            return false;
        }

        // Nos itens desse tipo, bota ele lá
        i.setEquiped(true);
        i.update();
        return equipados.get(i.getTipo()).add(i);
    }

    public boolean desequipar(Item i) {
        
        update();
        
        if (equipados.get(i.getTipo()).remove(i)) {
            i.setEquiped(true);
            i.update();
            return inventario.add(i);
        }

        // Se ele não achou o item equipado,
        // deu errado
        return false;
    }

    public boolean equals(Object o) {
        Jogador jog = (Jogador) o;
        return jog.getDest() == this.getDest()
                && jog.getInte() == this.getInte()
                && jog.getForca() == this.getForca();
    }

    @Override
    public boolean insert() {
        String selectSQL = "INSERT INTO jogador VALUES(JOGADOR_SEQ.NEXTVAL, ?, ?, ?, ?, ?, ?, ?, ?)";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, money);
            ps.setInt(2, forca);
            ps.setInt(3, dest);
            ps.setInt(4, inte);
            ps.setInt(5, vidaAtual);
            ps.setInt(6, manaAtual);
            ps.setInt(7, vidaMaxima);
            ps.setInt(8, manaMaxima);
            ps.executeUpdate();

            ps = dbConnection.prepareStatement("SELECT JOGADOR_SEQ.CURRVAL FROM DUAL");
            ResultSet rs = ps.executeQuery();
            rs.next();
            id = rs.getInt("CURRVAL");

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return true;
    }

    @Override
    public boolean delete() {
        String selectSQL = "DELETE FROM jogador WHERE id = ?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {
            
            for (Item i : inventario) {
                i.delete();
            }
            
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, id);

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return true;
    }

    @Override
    public boolean update() {

        if (id < 0) {
            return insert();
        }

        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        String insertTableSQL = "INSERT INTO itens_jogador VALUES(?, ?)";
        String deleteTableSQL = "DELETE FROM itens_loja WHERE id_loja = ? AND id_item = ?";

        try {

            System.out.println("Salvar dados jogador");

            ps = dbConnection.prepareStatement("update jogador set id=?, money=?, forca=?, dest=?, inte=?, vida_atual=?, mana_atual=?, vida_maxima=?, mana_maxima=? WHERE id = ?");
            ps.setInt(1, id);
            ps.setInt(2, money);
            ps.setInt(3, forca);
            ps.setInt(4, dest);
            ps.setInt(5, inte);
            ps.setInt(6, vidaAtual);
            ps.setInt(7, manaAtual);
            ps.setInt(8, vidaMaxima);
            ps.setInt(9, manaMaxima);
            ps.setInt(10, id);

            ps.executeUpdate();

            for (Item i : inventarioOffline) {
                System.out.println(i.getNome());
                try {

                    // Tira da loja
                    if (i.getId() > 0) {
                        ps = dbConnection.prepareStatement(deleteTableSQL);
                        ps.setInt(1, this.id);
                        ps.setInt(2, i.getId());
                        ps.executeUpdate();
                    }

                    i.update();

                    // Pega para o jogador
                    ps = dbConnection.prepareStatement(insertTableSQL);
                    ps.setInt(1, this.id);
                    ps.setInt(2, i.getId());

                    ps.executeUpdate();

                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        inventario.addAll(inventarioOffline);
        inventarioOffline.clear();

        System.out.println("fim jogador");

        return true;
    }

    @Override
    public void load() {
        String selectSQL = "SELECT * FROM jogador WHERE id = ?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, this.id);

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                setMoney(rs.getInt("money"));
                setForca(rs.getInt("forca"));
                setDest(rs.getInt("dest"));
                setInte(rs.getInt("inte"));
                setVidaAtual(rs.getInt("vida_atual"));
                setManaAtual(rs.getInt("mana_atual"));
                setVidaMaxima(rs.getInt("vida_maxima"));
                setManaMaxima(rs.getInt("mana_maxima"));

                loadItens(c);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void load(int id) {
        setId(id);
        load();
    }

    private void loadItens(Conexao c) {
        String selectSQL = "SELECT * FROM itens_jogador where id_jogador = ?";

        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, this.id);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Item i = new Item();
                i.load(rs.getInt("id_item"));
                inventario.add(i);
                
                if(i.isEquiped()) {
                    this.equipados.get(i.getTipo()).add(i);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
