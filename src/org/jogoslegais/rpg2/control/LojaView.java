/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jogoslegais.rpg2.control;

import java.io.IOException;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 *
 * @author Larissa
 */
public class LojaView extends Application {
    
    public static void main (String[] args){
    launch(args);
    }
    
    private Stage primaryStage;

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Loja Maneira");
        primaryStage.setScene(getRootLayout());
        primaryStage.show();
    }

    private Scene getRootLayout() {
       Scene scene = null;
        try {
            // Loader
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(LojaView.class.getResource("Loja.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();
            
            scene = new Scene(rootLayout);
            
        } catch (IOException e) {
            System.out.println("oi");
            e.printStackTrace();
        }
        return scene; //To change body of generated methods, choose Tools | Templates.
    }
    
    @FXML
    public void exitApplication(ActionEvent event) {
       
        
        
        Platform.exit();
    }
    
}
