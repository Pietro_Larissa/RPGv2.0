/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jogoslegais.rpg2.model.itens;

public enum ItemType
{
    Anel,
    Armadura,
    Pocao;
    
    public static ItemType fromInt(int i)
    {
        return values()[i];
    }

    public int toInt()
    {
        ItemType[] arr = values();

        for(int i = 0; i < arr.length; i++)
        {
            if(this == arr[i])
                return i;
        }
        
        ItemType.Armadura.toInt();
        
        Item sla = new Item();
        
        sla.setTipo(ItemType.Armadura);
        
        return -1;
    }
    
    @Override
    public String toString() {
        
        switch(this) {
            case Anel:
                return "Anel";
            case Armadura:
                return "Armadura";
            case Pocao:
                return "Poção";
        }
        
        return "";
    }
}