/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jogoslegais.rpg2.model.itens;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList; 
import javafx.scene.control.ListView;
import org.bigolin.ActiveRecord;
import org.bigolin.Conexao;
import org.jogoslegais.rpg2.model.jogador.Jogador;

/**
 *
 * @author aluno
 */
public class Loja implements ActiveRecord {

    // Itens a venda
    private ArrayList<Item> itens = new ArrayList<Item>();
    // Itens a venda, ainda não no BD
    private ArrayList<Item> itensOffline = new ArrayList<Item>();

    private Jogador jog;

    private int id = -1;

    public Loja() {
        for (int i = 0; i < 3; i++) {
            itens.add(Item.random());
        }
    }

    public void addItem(Item i) {
        itensOffline.add(i);
    }

    public void removeItem(Item i) {
        itensOffline.remove(i);
        itens.remove(i);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean vender(Item i) {
        // Se o jogador não tem o item,
        // ele não pode vender
        if (!jog.getInventario().contains(i)) {
            return false;
        }

        // Vende pela metade do preco
        jog.setMoney(jog.getMoney() + i.getMoney() / 2);

        addItem(i);

        return true;
    }

    // retorna se a compra deu certo
    public boolean comprar(Item i) {
        // Se o jogador não tem dinheiro,
        // deu ruim
        if (jog.getMoney() < i.getMoney()) {
            return false;
        }

        // Se o item não está na loja,
        // deu ruim
        if (!getItens().contains(i)) {
            return false;
        }

        jog.setMoney(jog.getMoney() - i.getMoney());
        jog.addItem(i);
        this.removeItem(i);
        return true;
    }

    public ArrayList<Item> getItens() {
        ArrayList<Item> res = new ArrayList<>();

        res.addAll(itens);
        res.addAll(itensOffline);

        return res;
    }

    public Jogador getJog() {
        return jog;
    }

    public void setJog(Jogador jog) {
        this.jog = jog;
    }

    @Override
    public boolean insert() {
        String selectSQL = "INSERT INTO loja VALUES(LOJA_SEQ.NEXTVAL)";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.executeUpdate();

            ps = dbConnection.prepareStatement("SELECT LOJA_SEQ.CURRVAL FROM DUAL");
            ResultSet rs = ps.executeQuery();
            rs.next();
            id = rs.getInt("CURRVAL");

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return true;
    }

    @Override
    public boolean delete() {
        String selectSQL = "DELETE FROM loja WHERE id = ?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {

            for (Item i : itens) {
                i.delete();
            }

            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, id);

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return true;
    }

    @Override
    public boolean update() {

        if (id < 0) {
            insert();
        }

        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        String insertTableSQL = "INSERT INTO itens_loja VALUES(?, ?)";
        String deleteTableSQL = "DELETE FROM itens_jogador WHERE id_jogador = ? AND id_item = ?";

        System.out.println("inicio dados loja");

        for (Item i : itensOffline) {
            System.out.println(i.getNome());
            try {
                // Tira do jogador, mas só tira
                // o item se ele já está no bd
                if (i.getId() >= 0) {
                    ps = dbConnection.prepareStatement(deleteTableSQL);
                    ps.setInt(1, jog.getId());
                    ps.setInt(2, i.getId());

                    ps.executeUpdate();
                }

                i.update();

                // Pega para a loja
                ps = dbConnection.prepareStatement(insertTableSQL);
                ps.setInt(1, this.id);
                ps.setInt(2, i.getId());

                ps.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        c.desconecta();

        itens.addAll(itensOffline);
        itensOffline.clear();

        System.out.println("fim dados loja");

        return true;
    }

    @Override
    public void load() {
        String selectSQL = "SELECT * FROM itens_loja where id_loja = ?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, this.id);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Item i = new Item();
                i.load(rs.getInt("id_item"));
                itens.add(i);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void load(int id) {
        setId(id);
        load();
    }
}
